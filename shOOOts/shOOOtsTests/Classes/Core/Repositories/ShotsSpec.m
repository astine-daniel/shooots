//
//  ShotsSpec.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright 2015 Daniel Astine. All rights reserved.
//

// Class under test
#import "Shots.h"

// Collaborators
#import "ShotAPI.h"
#import "Shot.h"
#import "Image.h"
#import "User.h"

// Test support
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>

Shot* createShot() {
    NSURL *url = [NSURL URLWithString:@"https://dribbble.com/"];
    Image *image = [[Image alloc] initWithImageURL:url teaserURL:url HiDPIURL:url];
    User *user = [[User alloc] initWithIdentifier:1 name:@"" avatarURL:url];
    
    return [[Shot alloc] initWithIdentifier:1 title:@"" shotDescription:@"" numberOfViews:1 image:image user:user];
}

SpecBegin(Shots)

describe(@"Shots", ^{
    __block Shots *shots;
    __block id<ShotAPI> api;
    
    beforeEach(^{
        api = OCMProtocolMock(@protocol(ShotAPI));
        shots = [[Shots alloc] initWithAPI:api];
    });
    
    context(@"when fetch for shots", ^{
        it(@"should return a list of shots", ^{
            NSArray *listOfShots = @[createShot(), createShot(), createShot()];
            
            OCMExpect([api fetchShotsFromPage:1
                                      success:[OCMArg any]
                                      failure:[OCMArg any]]).andDo(^ (NSInvocation *invocation) {
                void (^success)(NSArray *shots) = nil;
                [invocation getArgument:&success atIndex:3];
                success(listOfShots);
            });
            
            waitUntil(^(DoneCallback done) {
                void(^success)(NSArray *) = ^(NSArray *shotsResponse) {
                    expect(shotsResponse).toNot.beNil();
                    expect(shotsResponse).to.haveACountOf(3);
                    expect(shotsResponse).to.equal(listOfShots);
                    
                    done();
                };
                
                [shots fetchShotsWithSuccess:success failure:[OCMArg any]];
            });
       });
        
       it(@"should return an erro in case of failed", ^{
           NSError *error = [NSError errorWithDomain:@"shots" code:1 userInfo:nil];
           
           OCMExpect([api fetchShotsFromPage:1
                                     success:[OCMArg any]
                                     failure:[OCMArg any]]).andDo(^ (NSInvocation *invocation) {
               void (^failureCallback)(NSError *error) = nil;
               [invocation getArgument:&failureCallback atIndex:4];
               failureCallback(error);
           });
           
           waitUntil(^(DoneCallback done) {
               void(^failureCallback)(NSError *) = ^(NSError *errorResponse) {
                   expect(errorResponse).toNot.beNil();
                   expect(errorResponse).to.equal(error);
                   
                   done();
               };
               
               [shots fetchShotsWithSuccess:[OCMArg any] failure:failureCallback];
           });
       });
    });
    
    context(@"when remove all shots", ^{
       it(@"should return an empty list of shots", ^{
           NSArray *listOfShots = @[createShot(), createShot(), createShot()];
           
           OCMExpect([api fetchShotsFromPage:1
                                     success:[OCMArg any]
                                     failure:[OCMArg any]]).andDo(^ (NSInvocation *invocation) {
               void (^success)(NSArray *shots) = nil;
               [invocation getArgument:&success atIndex:3];
               success(listOfShots);
           });
           
           waitUntil(^(DoneCallback done) {
               void(^success)(NSArray *) = ^(NSArray *shotsResponse) {
                   [shots removeAllShots];
                   expect(shots.all).to.beEmpty();

                   done();
               };
               
               [shots fetchShotsWithSuccess:success failure:[OCMArg any]];
           });
       });
    });
});

SpecEnd
