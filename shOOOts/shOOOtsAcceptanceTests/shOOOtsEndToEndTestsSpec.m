//
//  shOOOtsEndToEndTestsSpec.m
//  shOOOts
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright 2015 Daniel Astine. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <KIF/KIF.h>

SpecBegin(shOOOts)

context(@"when application open", ^{
   it(@"should show a collection of Shots", ^{
       UICollectionView *shotsCollectionView = (UICollectionView *)[tester waitForViewWithAccessibilityLabel:NSLocalizedString(@"Collection of Shots", nil)];
       NSUInteger numberOfShots = [shotsCollectionView numberOfItemsInSection:0];
       expect(numberOfShots).to.beGreaterThanOrEqualTo(1);
   });
});

SpecEnd
