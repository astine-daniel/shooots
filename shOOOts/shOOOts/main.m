//
//  main.m
//  shOOOts
//
//  Created by Daniel Astine on 11/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
