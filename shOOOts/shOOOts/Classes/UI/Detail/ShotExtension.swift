//
//  ShotExtension.swift
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

import UIKit

extension ShotDetailTableViewCell {
    func configure(with shot: Shot) {
        if var description = shot.shotDescription {
            do {
                let font = self.descriptionLabel.font
                description.appendContentsOf("<style>body{font-family: \(font.fontName); font-size:\(font.pointSize);}</style>")
                
                try self.descriptionLabel?.attributedText = NSAttributedString(data: description.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!,
                    options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
            }
            catch {
                
            }
        } else {
            self.descriptionLabel.text = ""
        }
    }
}

extension ShotDetailTableViewHeader {
    func configure(with shot: Shot) {
        if let URL = shot.image.HiDPIURL {
            self.coverImageView.sd_setImageWithURL(URL)
        } else {
            self.coverImageView.sd_setImageWithURL(shot.image.URL)
        }
        
        
        
        self.titleLabel.text = shot.title
        self.numberOfViewsLabel.text = "\(shot.numberOfViews)"
    }
}

extension ShotDetailTableViewSectionHeader {
    func configure(with user: User) {
        self.avatarImageView.sd_setImageWithURL(user.avatarURL)
        self.nameLabel.text = user.name
    }
}
