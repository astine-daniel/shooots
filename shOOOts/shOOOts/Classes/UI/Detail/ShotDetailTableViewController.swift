//
//  ShotDetailTableViewController.swift
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

import UIKit
import Social

public class ShotDetailTableViewController: UITableViewController {
    private let cellIdentifier = "ShotDetailCell"
    
    @IBOutlet weak var tableHeaderView: ShotDetailTableViewHeader!
    @IBOutlet var sectionHeaderView: ShotDetailTableViewSectionHeader!
    
    public var shot: Shot?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = self.shot?.title
        self.navigationController?.hidesBarsOnSwipe = false;
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.configure()
        self.addShareButtons()
    }
    
    private func configure() {
        if let shot = self.shot {
            self.tableHeaderView.configure(with: shot)
            self.sectionHeaderView.configure(with: shot.user)
        }
    }
    
    private func addShareButtons() {
        let iconSize: CGFloat = 25
        let imageSize = CGSizeMake(44, 44)
        let color = self.view.tintColor
        
        let twitterImage = Icons.twitterIconWithSize(iconSize, imageSize: imageSize, color: color)
        let facebookImage = Icons.facebookIconWithSize(iconSize, imageSize: imageSize, color: color)
        
        let twitterButton = UIBarButtonItem(image: twitterImage, style: .Done, target: self, action: "tweet")
        let facebookButton = UIBarButtonItem(image: facebookImage, style: .Plain, target: self, action: "postOnFacebook")
        
        self.navigationItem.setRightBarButtonItems([twitterButton, facebookButton], animated: true)
    }
    
    func tweet() {
        let image = self.tableHeaderView.coverImageView.image
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            let tweetSweet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            tweetSweet.setInitialText("Great shOOOt")
            tweetSweet.addImage(image)
            [self .presentViewController(tweetSweet, animated: true, completion: nil)]
        }
        else {
            self.showAlert(with: "Please configure yout Twitter account at Settings!")
        }
    }
    
    func postOnFacebook() {
        let image = self.tableHeaderView.coverImageView.image
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            let facebookSweet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSweet.setInitialText("Great shOOOt")
            facebookSweet.addImage(image)
            [self .presentViewController(facebookSweet, animated: true, completion: nil)]
        }
        else {
            self.showAlert(with: "Please configure yout Facebook account at Settings!")
        }
    }
    
    private func showAlert(with text:String) {
        let alertController = UIAlertController(title: "shOOOts", message: text, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    //MARK: UITableViewDataSource
    
    override public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier, forIndexPath: indexPath) as! ShotDetailTableViewCell
        if let shot = self.shot {
            cell.configure(with: shot)
        }
        
        return cell
    }
    
    //MARK: UITableViewDelegate
    override public func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.sectionHeaderView
    }
    
    override public func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.sectionHeaderView.bounds.size.height
    }
    
    override public func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
}
