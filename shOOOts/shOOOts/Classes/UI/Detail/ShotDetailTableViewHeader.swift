//
//  ShotDetailTableViewHeader.swift
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

import UIKit

class ShotDetailTableViewHeader: UIView {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var viewsIconImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberOfViewsLabel: UILabel!
    
    override func layoutSubviews() {
        self.setViewsIcon()
        
        super.layoutSubviews()
    }
    
    private func setViewsIcon() {
        if self.viewsIconImageView.image == nil {
            let size = self.viewsIconImageView.bounds.size
            let image = Icons.eyeIconWithSize(30, imageSize: size, color: UIColor.whiteColor());
            self.viewsIconImageView.image = image
        }
    }
}
