//
//  ShotDetailTableViewCell.swift
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

import UIKit

class ShotDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
}
