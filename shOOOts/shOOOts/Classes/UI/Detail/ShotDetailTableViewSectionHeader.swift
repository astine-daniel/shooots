//
//  ShotDetailTableViewSectionHeader.swift
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

import UIKit

class ShotDetailTableViewSectionHeader: UIView {
    @IBOutlet weak var avatarImageView: CircularImageView!
    @IBOutlet weak var nameLabel: UILabel!
}
