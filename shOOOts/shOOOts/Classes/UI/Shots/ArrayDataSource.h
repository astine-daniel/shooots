//
//  ArrayDataSource.h
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@interface ArrayDataSource<ObjectType> : NSObject

NS_ASSUME_NONNULL_BEGIN

typedef void(^ConfigureBlock)(id itemContainer, ObjectType item);

- (instancetype)initWithItems:(NSArray<ObjectType> *)items
itemContainerIdentifier:(NSString *)identifier
configureBlock:(ConfigureBlock)configureBlock;

- (instancetype)initWithItemContainerIdentifier:(NSString *)identifier
configureBlock:(ConfigureBlock)configureBlock;

@property (nonatomic, strong, readonly) NSArray<ObjectType> *items;
@property (nonatomic, strong, readonly) NSString *itemContentIdentifier;
@property (nonatomic, strong, readonly) ConfigureBlock configureBlock;

- (void)addItem:(ObjectType)item;
- (void)addItems:(NSArray<ObjectType> *)items;

- (void)removeAllItems;

NS_ASSUME_NONNULL_END

@end
