//
//  ShotCollectionViewCell+ConfigureForShot.m
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "ShotCollectionViewCell+ConfigureForShot.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "Shot.h"
#import "Image.h"

@implementation ShotCollectionViewCell (ConfigureForShot)

- (void)configureForShot:(Shot *)shot {
    [self.coverImageView sd_setImageWithURL:shot.image.teaserURL];
    
    self.titleLabel.text = shot.title;
    self.numberOfViewsLabel.text = [NSString stringWithFormat:@"%i", (int)shot.numberOfViews];
}

@end
