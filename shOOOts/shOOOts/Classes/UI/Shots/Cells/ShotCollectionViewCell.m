//
//  ShotCollectionViewCell.m
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "ShotCollectionViewCell.h"
#import "Icons.h"

@implementation ShotCollectionViewCell

+ (CGSize)cellSize {
    return CGSizeMake(300.0f, 225.0f);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupViewIconImageView];
}

- (void)setupViewIconImageView {
    CGSize imageSize = self.viewIconImageView.frame.size;
    UIImage *image = [Icons eyeIconWithSize:30.0f imageSize:imageSize color:[UIColor whiteColor]];
    self.viewIconImageView.image = image;
}

@end
