//
//  ShotCollectionViewCell.h
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import UIKit;

@interface ShotCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *viewIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfViewsLabel;

+ (CGSize)cellSize;

@end
