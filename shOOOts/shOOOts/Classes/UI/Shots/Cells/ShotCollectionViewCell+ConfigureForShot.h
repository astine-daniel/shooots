//
//  ShotCollectionViewCell+ConfigureForShot.h
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "ShotCollectionViewCell.h"

@class Shot;

@interface ShotCollectionViewCell (ConfigureForShot)

NS_ASSUME_NONNULL_BEGIN

- (void)configureForShot:(Shot *)shot;

NS_ASSUME_NONNULL_END

@end
