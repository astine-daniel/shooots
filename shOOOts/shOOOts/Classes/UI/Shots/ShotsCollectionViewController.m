//
//  ShotsCollectionViewController.m
//  shOOOts
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "ShotsCollectionViewController.h"

#import <UIScrollView_InfiniteScroll/UIScrollView+InfiniteScroll.h>

#import "CollectionViewArrayDataSource.h"
#import "ShotCollectionViewCell+ConfigureForShot.h"

#import "Shots.h"
#import "Shot.h"

#import "DribbbleAPI+AccessToken.h"
#import "DribbbleAPI+ShotAPI.h"

#import "shOOOts-Swift.h"

@interface ShotsCollectionViewController () <UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) CollectionViewArrayDataSource<Shot *> *collectionViewDataSource;
@property (nonatomic, strong) Shots *shots;

@property (nonatomic, strong) Shot *selectedShot;

@end

@implementation ShotsCollectionViewController

static NSString * const kShotsViewControllerStoryboardIdentifier = @"ShotsCollectionViewController";
static NSString * const kShotsViewControllerCollectionCellIdentifier = @"ShotCollectionCell";
static NSString * const kTitle = @"Shots";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupCollectionView];
    [self refreshShots];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = [kTitle localizedString];
    self.navigationController.hidesBarsOnSwipe = YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ShotDetailTableViewController *detailViewController = (ShotDetailTableViewController *)segue.destinationViewController;
    detailViewController.shot = self.selectedShot;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedShot = [self.collectionViewDataSource itemAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"showDetail" sender:nil];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 15.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [ShotCollectionViewCell cellSize];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0.0f, [self margin], 15.0f, [self margin]);
}

- (CGFloat)margin {
    CGFloat cellWidth = [ShotCollectionViewCell cellSize].width;
    CGFloat collectionViewWidth = CGRectGetWidth(self.view.bounds);
    NSInteger numberOFItemsPerRow = (NSInteger)(collectionViewWidth/cellWidth);
    CGFloat margin = (collectionViewWidth - (numberOFItemsPerRow * cellWidth)) / (numberOFItemsPerRow + 1);
    
    return margin;
}

#pragma mark - Private methods

- (void)setupCollectionView {
    self.collectionView.dataSource = self.collectionViewDataSource;
    [self addRefreshControl];
}

- (void)addRefreshControl {
    [self.refreshControl addTarget:self action:@selector(refreshShots) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
}

- (void)addInfiniteScroll {
    @weakify(self);
    [self.collectionView addInfiniteScrollWithHandler:^(UICollectionView *collectionView) {
        @strongify(self);
        [self loadMoreShots];
    }];
}

- (void)refreshShots {
    [self addInfiniteScroll];
    
    [self.collectionViewDataSource removeAllItems];
    
    [self.shots removeAllShots];
    [self.shots fetchShotsWithSuccess:[self addShots]
                              failure:[self handleErrorOfFetchingShots]];
}

- (void)loadMoreShots {
    [self.shots fetchShotsWithSuccess:[self addShots]
                              failure:[self handleErrorOfFetchingShots]];
}

- (UIRefreshControl *)refreshControl {
    if (_refreshControl == nil) {
        _refreshControl = [[UIRefreshControl alloc] init];
    }
    
    return _refreshControl;
}

- (CollectionViewArrayDataSource *)collectionViewDataSource {
    if (_collectionViewDataSource == nil) {
        _collectionViewDataSource = [[CollectionViewArrayDataSource alloc]
                                     initWithItemContainerIdentifier:kShotsViewControllerCollectionCellIdentifier
                                     configureBlock:[self configureBlock]];
        _collectionViewDataSource.collectionView = self.collectionView;
    }
    
    return _collectionViewDataSource;
}

- (ConfigureBlock)configureBlock {
    return ^(ShotCollectionViewCell *cell, Shot *shot) {
        [cell configureForShot:shot];
    };
}

- (void (^)(NSArray<Shot *>*))addShots {
    return ^(NSArray<Shot *>* newShots) {
        if ([newShots count] == 0) {
            [self.collectionView removeInfiniteScroll];
        }
        
        [self.collectionViewDataSource addItems:newShots];
        
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        
        [self.collectionView finishInfiniteScroll];
    };
}

- (void (^)(NSError*))handleErrorOfFetchingShots {
    return ^(NSError *error) {
        DDLogError(@"%@", error);
    };
}

- (Shots *)shots {
    if (_shots == nil) {
        DribbbleAPI *api = [[DribbbleAPI alloc] initWithAccessToken:[DribbbleAPI accessToken]];
        _shots = [[Shots alloc] initWithAPI:api];
    }
    
    return _shots;
}

@end
