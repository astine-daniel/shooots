//
//  CollectionViewArrayDataSource.h
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "ArrayDataSource.h"

@import UIKit;

@interface CollectionViewArrayDataSource<ObjectType> : ArrayDataSource<ObjectType> <UICollectionViewDataSource>

NS_ASSUME_NONNULL_BEGIN

@property (nonatomic, weak) UICollectionView *collectionView;

- (ObjectType)itemAtIndexPath:(NSIndexPath *)indexPath;
- (void)removeAllItems;

NS_ASSUME_NONNULL_END

@end
