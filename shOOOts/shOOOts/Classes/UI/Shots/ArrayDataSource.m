//
//  ArrayDataSource.m
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "ArrayDataSource.h"

@interface ArrayDataSource ()

@property (nonatomic, strong) NSMutableArray *elements;
@property (nonatomic, strong, readwrite) NSString *itemContentIdentifier;
@property (nonatomic, strong, readwrite) ConfigureBlock configureBlock;

@end

@implementation ArrayDataSource

- (instancetype)initWithItems:(NSArray *)items
      itemContainerIdentifier:(NSString *)identifier
               configureBlock:(ConfigureBlock)configureBlock {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.elements = [NSMutableArray arrayWithArray:items];
    self.itemContentIdentifier = identifier;
    self.configureBlock = configureBlock;
    
    return self;
}

- (instancetype)initWithItemContainerIdentifier:(NSString *)identifier configureBlock:(ConfigureBlock)configureBlock {
    return [self initWithItems:@[] itemContainerIdentifier:identifier configureBlock:configureBlock];
}

- (void)addItems:(NSArray *)items {
    [items each:^(id item) {
        [self addItem:item];
    }];
}

- (void)addItem:(id)item {
    [self.elements addObject:item];
}

- (NSArray *)items {
    return [self.elements copy];
}

- (void)removeAllItems {
    [self.elements removeAllObjects];
}

@end
