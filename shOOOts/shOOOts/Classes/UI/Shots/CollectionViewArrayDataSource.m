//
//  CollectionViewArrayDataSource.m
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "CollectionViewArrayDataSource.h"

@implementation CollectionViewArrayDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.itemContentIdentifier
                                                                           forIndexPath:indexPath];
    
    id item = [self itemAtIndexPath:indexPath];
    self.configureBlock(cell, item);
    
    return cell;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    return self.items[(NSUInteger)indexPath.row];
}

- (void)removeAllItems {
    NSArray *items = self.items;
    [super removeAllItems];
    
    NSArray *indexPaths = [self indexPathForItems:items];
    [self.collectionView performBatchUpdates:^{
        [self.collectionView deleteItemsAtIndexPaths:indexPaths];
    } completion:nil];
}

- (void)addItems:(NSArray *)items {
    NSArray *indexPaths = [self indexPathForItems:items];
    [super addItems:items];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView insertItemsAtIndexPaths:indexPaths];
    } completion:nil];
}

- (NSArray<NSIndexPath *> *)indexPathForItems:(NSArray *)items {
    NSMutableArray<NSIndexPath *> *indexPaths = [NSMutableArray array];
    
    NSUInteger currentNumberOfItems = [self.items count];
    [items eachWithIndex:^(id object, NSUInteger index) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:(currentNumberOfItems + index) inSection:0];
        [indexPaths addObject:indexPath];
    }];
    
    return [indexPaths copy];
}

@end
