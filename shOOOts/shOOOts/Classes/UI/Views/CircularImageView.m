//
//  CircularImageView.m
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "CircularImageView.h"

@implementation CircularImageView

- (void)layoutSubviews {
    self.layer.cornerRadius = CGRectGetWidth(self.bounds) / 2.0f;
    self.layer.masksToBounds = YES;
    
    [super layoutSubviews];
}

@end
