//
//  CircularImageView.h
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import UIKit;

IB_DESIGNABLE
@interface CircularImageView : UIImageView



@end
