//
//  AppDelegate.h
//  shOOOts
//
//  Created by Daniel Astine on 11/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

