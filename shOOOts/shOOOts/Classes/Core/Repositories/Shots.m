//
//  Shots.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "Shots.h"

#import "ShotAPI.h"

@interface Shots ()

@property (nonatomic, strong) id<ShotAPI> api;
@property (nonatomic, assign) NSUInteger page;

@property (nonatomic, strong) NSMutableArray<Shot *> *shots;

@end

@implementation Shots

- (instancetype)initWithAPI:(id<ShotAPI>)api {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.api = api;
    self.page = 1;
    
    return self;
}

- (NSMutableArray *)shots {
    if (_shots == nil) {
        _shots = [NSMutableArray array];
    }
    
    return _shots;
}

- (NSArray<Shot *> *)all {
    return [self.shots copy];
}

- (void)fetchShotsWithSuccess:(void (^)(NSArray<Shot *> * _Nonnull))success
                      failure:(void (^)(NSError * _Nonnull))failure {
    
    [self.api fetchShotsFromPage:self.page
                         success:^(NSArray<Shot *> * _Nonnull shots) {
                             [self addShots:shots];
                             success(shots);
                             
                         } failure:^(NSError * _Nonnull error) {
                             failure(error);
                         }];
    ++self.page;
}

- (void)removeAllShots {
    [self.shots removeAllObjects];
    self.page = 1;
}

- (void)addShots:(NSArray<Shot *> *)shots {
    [shots each:^(Shot *shot) {
        if ([self.shots containsObject:shot] == NO) {
            [self.shots addObject:shot];
        }
    }];
}

@end
