//
//  Shots.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@protocol ShotAPI;
@class Shot;

@interface Shots : NSObject

NS_ASSUME_NONNULL_BEGIN

- (instancetype) __unavailable init;
- (instancetype)initWithAPI:(id<ShotAPI>)api;

- (void)fetchShotsWithSuccess:(void (^)(NSArray<Shot *> *shots))success failure:(void (^)(NSError* error))failure;
- (void)removeAllShots;

@property (nonatomic, copy, readonly) NSArray<Shot *> *all;

NS_ASSUME_NONNULL_END

@end
