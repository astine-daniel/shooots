//
//  ShotAPI.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@class Shot;

@protocol ShotAPI <NSObject>

NS_ASSUME_NONNULL_BEGIN

- (void)fetchShotsFromPage:(NSUInteger)page
                   success:(void (^)(NSArray<Shot *> *shots))success
                   failure:(void (^)(NSError* error))failure;

NS_ASSUME_NONNULL_END

@end
