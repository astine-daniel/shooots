//
//  MTLJSONAdapter+Transformers.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MTLJSONAdapter (Transformers)

+ (NSValueTransformer<MTLTransformerErrorHandling> *)unsignedIntegerTransformer;

@end
