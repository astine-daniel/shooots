//
//  MTLJSONAdapter+Transformers.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "MTLJSONAdapter+Transformers.h"

@implementation MTLJSONAdapter (Transformers)

+ (NSValueTransformer<MTLTransformerErrorHandling> *)unsignedIntegerTransformer {
    return [MTLValueTransformer
            transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
                return @([((NSString *)value) unsignedIntegerValue]);
            } reverseBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
                return value;
            }];
}

@end
