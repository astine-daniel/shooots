//
//  NSString+UnsignedIntegerValue.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@interface NSString (UnsignedIntegerValue)

- (NSUInteger)unsignedIntegerValue;

@end
