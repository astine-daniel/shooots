//
//  NSString+Localized.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "NSString+Localized.h"

@implementation NSString (Localized)

- (NSString *)localizedString {
    return [self localizedStringWithComment:nil];
}

- (NSString *)localizedStringWithComment:(NSString *)comment {
    return NSLocalizedString(self, comment);
}

@end
