//
//  NSString+UnsignedIntegerValue.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "NSString+UnsignedIntegerValue.h"

@implementation NSString (UnsignedIntegerValue)

- (NSUInteger)unsignedIntegerValue {
    NSInteger value = [self integerValue];
    if (value < 0) {
        return 0;
    }
    
    return value;
}

@end
