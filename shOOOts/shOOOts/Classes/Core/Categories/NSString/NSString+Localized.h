//
//  NSString+Localized.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@interface NSString (Localized)

NS_ASSUME_NONNULL_BEGIN

- (NSString *)localizedString;
- (NSString *)localizedStringWithComment:(NSString * _Nullable)comment;

NS_ASSUME_NONNULL_END

@end
