//
//  Icons.h
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import UIKit;

@interface Icons : NSObject

NS_ASSUME_NONNULL_BEGIN

+ (UIImage *)eyeIconWithSize:(CGFloat)size imageSize:(CGSize)imageSize color:(UIColor *)color;
+ (UIImage *)twitterIconWithSize:(CGFloat)size imageSize:(CGSize)imageSize color:(UIColor *)color;
+ (UIImage *)facebookIconWithSize:(CGFloat)size imageSize:(CGSize)imageSize color:(UIColor *)color;

NS_ASSUME_NONNULL_END

@end
