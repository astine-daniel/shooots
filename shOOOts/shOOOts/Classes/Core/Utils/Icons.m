//
//  Icons.m
//  shOOOts
//
//  Created by Daniel Astine on 14/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "Icons.h"
#import <ionicons/IonIcons.h>

@implementation Icons

+ (UIImage *)eyeIconWithSize:(CGFloat)size imageSize:(CGSize)imageSize color:(UIColor *)color {
    return [IonIcons imageWithIcon:ion_ios_eye
                         iconColor:color
                          iconSize:size
                         imageSize:imageSize];
}

+ (UIImage *)twitterIconWithSize:(CGFloat)size imageSize:(CGSize)imageSize color:(UIColor *)color {
    return [IonIcons imageWithIcon:ion_social_twitter
                         iconColor:color
                          iconSize:size
                         imageSize:imageSize];
}

+ (UIImage *)facebookIconWithSize:(CGFloat)size imageSize:(CGSize)imageSize color:(UIColor *)color {
    return [IonIcons imageWithIcon:ion_social_facebook
                         iconColor:color
                          iconSize:size
                         imageSize:imageSize];
}

@end
