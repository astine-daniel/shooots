//
//  Image.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "Image.h"

@interface Image ()

@property (nonatomic, strong, readwrite) NSURL *HiDPIURL;
@property (nonatomic, strong, readwrite) NSURL *URL;
@property (nonatomic, strong, readwrite) NSURL *teaserURL;

@end

@implementation Image

- (instancetype)initWithImageURL:(NSURL *)URL
                       teaserURL:(NSURL *)teaserURL
               HiDPIURL:(NSURL *)HiDPIURL {
    
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.URL = URL;
    self.teaserURL = teaserURL;
    self.HiDPIURL = HiDPIURL;
    
    return self;
}

- (BOOL)isEqual:(id)object {
    if ([self class] == [object class]) {
        return [self isEqualToImage:(Image *)object];
    }
    
    return [super isEqual:object];
}

- (BOOL)isEqualToImage:(Image *)otherImage {
    if (self == otherImage) {
        return YES;
    }
    
    BOOL equal = YES;
    
    equal &= [self.URL isEqual:otherImage.URL];
    equal &= [self.teaserURL isEqual:otherImage.teaserURL];
    equal &= [self.HiDPIURL isEqual:otherImage.HiDPIURL];
    
    return equal;
}

- (NSUInteger)hash {
    NSUInteger URLHash = [self.URL hash];
    NSUInteger teaserURLHash = [self.teaserURL hash];
    NSUInteger HiDPIURLHash = [self.HiDPIURL hash];
    
    return URLHash ^ teaserURLHash ^ HiDPIURLHash;
}

@end
