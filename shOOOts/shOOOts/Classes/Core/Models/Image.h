//
//  Image.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@interface Image : NSObject

NS_ASSUME_NONNULL_BEGIN

- (instancetype) __unavailable init;
- (instancetype)initWithImageURL:(NSURL *)URL teaserURL:(NSURL *)teaserURL HiDPIURL:(NSURL *)HiDPIURL;

@property (nonatomic, strong, readonly, nullable) NSURL *HiDPIURL;
@property (nonatomic, strong, readonly) NSURL *URL;
@property (nonatomic, strong, readonly) NSURL *teaserURL;

NS_ASSUME_NONNULL_END

- (BOOL)isEqualToImage:(Image * _Nullable)otherImage;

@end
