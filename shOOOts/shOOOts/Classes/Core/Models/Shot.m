//
//  Shot.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "Shot.h"

#import "Image.h"
#import "User.h"

@interface Shot ()

@property (nonatomic, assign, readwrite) NSUInteger identifier;
@property (nonatomic, copy, readwrite) NSString *title;
@property (nonatomic, copy, readwrite) NSString *shotDescription;
@property (nonatomic, assign, readwrite) NSUInteger numberOfViews;
@property (nonatomic, strong, readwrite) Image *image;
@property (nonatomic, strong, readwrite) User *user;

@end

@implementation Shot

- (instancetype)initWithIdentifier:(NSUInteger)identifier
                             title:(NSString *)title
                   shotDescription:(NSString *)shotDescription
                     numberOfViews:(NSUInteger)numberOfViews
                             image:(Image *)image
                              user:(User *)user {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.identifier = identifier;
    self.title = title;
    self.shotDescription = shotDescription;
    self.numberOfViews = numberOfViews;
    self.image = image;
    self.user = user;
    
    return self;
}

- (BOOL)isEqualToShot:(Shot *)otherShot {
    if (self == otherShot) {
        return YES;
    }
    
    BOOL equal = YES;
    
    equal &= (self.identifier == otherShot.identifier);
    equal &= [self.title isEqualToString:otherShot.title];
    equal &= [self.shotDescription isEqualToString:otherShot.shotDescription];
    equal &= (self.numberOfViews == otherShot.numberOfViews);
    equal &= [self.image isEqualToImage:otherShot.image];
    equal &= [self.user isEqualToUser:otherShot.user];
    
    return equal;
}

- (NSUInteger)hash {
    NSUInteger identifierHash = self.identifier;
    NSUInteger titleHash = [self.title hash];
    NSUInteger shotDescriptionHash = [self.shotDescription hash];
    NSUInteger numberOfViewsHash = self.numberOfViews;
    NSUInteger imageHash = [self.image hash];
    NSUInteger userHash = [self.user hash];
    
    return identifierHash ^ titleHash ^ shotDescriptionHash ^ numberOfViewsHash ^ imageHash ^ userHash;
}

@end
