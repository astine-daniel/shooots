//
//  Shot.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@class Image;
@class User;

@interface Shot : NSObject

NS_ASSUME_NONNULL_BEGIN

- (instancetype) __unavailable init;
- (instancetype)initWithIdentifier:(NSUInteger)identifier
                             title:(NSString *)title
                   shotDescription:(NSString * _Nullable)shotDescription
                     numberOfViews:(NSUInteger)numberOfViews
                             image:(Image *)image
                              user:(User *)user;

@property (nonatomic, assign, readonly) NSUInteger identifier;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly, nullable) NSString *shotDescription;
@property (nonatomic, assign, readonly) NSUInteger numberOfViews;
@property (nonatomic, strong, readonly) Image *image;
@property (nonatomic, strong, readonly) User *user;

NS_ASSUME_NONNULL_END

- (BOOL)isEqualToShot:(Shot * _Nullable)otherShot;

@end
