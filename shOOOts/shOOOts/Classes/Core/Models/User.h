//
//  User.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@interface User : NSObject

NS_ASSUME_NONNULL_BEGIN

- (instancetype) __unavailable init;
- (instancetype)initWithIdentifier:(NSUInteger)identifier name:(NSString *)name avatarURL:(NSURL *)avatarURL;

@property (nonatomic, assign, readonly) NSUInteger identifier;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSURL *avatarURL;


NS_ASSUME_NONNULL_END

- (BOOL)isEqualToUser:(User * _Nullable)otherUser;

@end
