//
//  User.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "User.h"

@interface User ()

@property (nonatomic, assign, readwrite) NSUInteger identifier;
@property (nonatomic, copy, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSURL *avatarURL;

@end

@implementation User

- (instancetype)initWithIdentifier:(NSUInteger)identifier name:(NSString *)name avatarURL:(NSURL *)avatarURL {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.identifier = identifier;
    self.name = name;
    self.avatarURL = avatarURL;
    
    return self;
}

- (BOOL)isEqual:(id)object {
    if ([self class] == [object class]) {
        return [self isEqualToUser:(User *)object];
    }
    
    return [super isEqual:object];
}

- (BOOL)isEqualToUser:(User *)otherUser {
    if (self == otherUser) {
        return YES;
    }
    
    BOOL equal = YES;
    
    equal &= (self.identifier == otherUser.identifier);
    equal &= [self.name isEqualToString:otherUser.name];
    equal &= [self.avatarURL isEqual:otherUser.avatarURL];
    
    return equal;
}

- (NSUInteger)hash {
    NSUInteger identifierHash = self.identifier;
    NSUInteger nameHash = [self.name hash];
    NSUInteger avatarURLHash = [self.avatarURL hash];
    
    return identifierHash ^ nameHash ^ avatarURLHash;
}

@end
