//
//  DribbbleAPI+ShotAPI.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleAPI+ShotAPI.h"

#import <Mantle/Mantle.h>

#import "Shot.h"
#import "DribbbleShot.h"

#import "ShotCollector.h"

@implementation DribbbleAPI (ShotAPI)

- (void)fetchShotsFromPage:(NSUInteger)page
                   success:(void (^)(NSArray<Shot *> * _Nonnull))success
                   failure:(void (^)(NSError * _Nonnull))failure {
    
    [self shotsFromPage:page
                success:^(NSArray *data) {
                    NSArray<Shot *>* shots = [self shotsFromData:data];
                    success(shots);
                } failure:^(NSError *error) {
                    failure(error);
                }];
}

- (NSArray<Shot *> *)shotsFromData:(NSArray *)data {
    NSError *error = nil;
    NSArray *dribbleShots = [MTLJSONAdapter modelsOfClass:[DribbbleShot class]
                                            fromJSONArray:data
                                                    error:&error];
    
    if (error) {
        DDLogError(@"%@", error);
        return @[];
    }
    
    ShotCollector *collector = [[ShotCollector alloc] init];
    [collector addDribbbleShots:dribbleShots];
    
    return [collector collectedShots];
}

@end
