//
//  ShotCollector.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "ShotCollector.h"

#import "Shot.h"
#import "Image.h"
#import "User.h"

#import "DribbbleShot.h"
#import "DribbbleImage.h"
#import "DribbbleUser.h"

@interface ShotCollector ()

@property (nonatomic, strong) NSMutableArray<Shot *> *shots;

@end

@implementation ShotCollector

- (void)addDribbbleShots:(NSArray<DribbbleShot *> *)dribbbleShots {
    [dribbbleShots each:^(DribbbleShot *dribbbleShot) {
        [self addDribbbleShot:dribbbleShot];
    }];
}

- (void)addDribbbleShot:(DribbbleShot *)dribbbleShot {
    Shot *shot = [self shotForDribbbleShot:dribbbleShot];
    [self addShot:shot];
}

- (Shot *)shotForDribbbleShot:(DribbbleShot *)dribbbleShot {
    Image *image = [self imageForDribbbleImage:dribbbleShot.image];
    User *user = [self userForDribbbleUser:dribbbleShot.user];
    
    return [[Shot alloc] initWithIdentifier:dribbbleShot.identifier
                                      title:dribbbleShot.title
                            shotDescription:dribbbleShot.shotDescription
                              numberOfViews:dribbbleShot.numberOfViews
                                      image:image
                                       user:user];
}

- (Image *)imageForDribbbleImage:(DribbbleImage *)dribbbleImage {
    return [[Image alloc] initWithImageURL:dribbbleImage.URL
                                 teaserURL:dribbbleImage.teaserURL
                                  HiDPIURL:dribbbleImage.HiDPIURL];
}

- (User *)userForDribbbleUser:(DribbbleUser *)dribbbleUser {
    return [[User alloc] initWithIdentifier:dribbbleUser.identifier
                                       name:dribbbleUser.name
                                  avatarURL:dribbbleUser.avatarURL];
}

- (void)addShot:(Shot *)shot {
    [self.shots addObject:shot];
}

- (NSArray<Shot *> *)collectedShots {
    return [self.shots copy];
}

- (NSMutableArray<Shot *> *)shots {
    if (_shots == nil) {
        _shots = [NSMutableArray array];
    }
    
    return _shots;
}

@end
