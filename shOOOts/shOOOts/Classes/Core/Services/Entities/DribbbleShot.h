//
//  DribbbleShot.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import <Mantle/Mantle.h>

@class DribbbleImage;
@class DribbbleUser;

@interface DribbbleShot : MTLModel <MTLJSONSerializing>

NS_ASSUME_NONNULL_BEGIN

@property (nonatomic, assign, readonly) NSUInteger identifier;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly, nullable) NSString *shotDescription;
@property (nonatomic, assign, readonly) NSUInteger numberOfViews;
@property (nonatomic, strong, readonly) DribbbleImage *image;
@property (nonatomic, strong, readonly) DribbbleUser *user;

NS_ASSUME_NONNULL_END

@end
