//
//  DribbbleShot.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleShot.h"

#import "DribbbleImage.h"
#import "DribbbleUser.h"

#import "MTLJSONAdapter+Transformers.h"

static NSString * const kDribbbleShotIdentifierJSONKey = @"id";
static NSString * const kDribbbleShotTitleJSONKey = @"title";
static NSString * const kDribbbleShotShotDescriptionJSONKey = @"description";
static NSString * const kDribbbleShotNumberOfViewsJSONKey = @"views_count";
static NSString * const kDribbbleShotImageJSONKey = @"images";
static NSString * const kDribbbleShotUserJSONKey = @"user";

@implementation DribbbleShot

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    DribbbleShot *shot = nil;
    return @{@keypath(shot.identifier): kDribbbleShotIdentifierJSONKey,
              @keypath(shot.title): kDribbbleShotTitleJSONKey,
              @keypath(shot.shotDescription): kDribbbleShotShotDescriptionJSONKey,
              @keypath(shot.numberOfViews): kDribbbleShotNumberOfViewsJSONKey,
              @keypath(shot.image): kDribbbleShotImageJSONKey,
              @keypath(shot.user): kDribbbleShotUserJSONKey};
}

+ (NSValueTransformer *)identifierJSONTransformer {
    return [MTLJSONAdapter unsignedIntegerTransformer];
}

+ (NSValueTransformer *)numberOfViewsJSONTransformer {
    return [MTLJSONAdapter unsignedIntegerTransformer];
}

+ (NSValueTransformer *)imageJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[DribbbleImage class]];
}

+ (NSValueTransformer *)userJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[DribbbleUser class]];
}

@end
