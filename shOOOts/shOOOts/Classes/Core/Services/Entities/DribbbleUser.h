//
//  DribbbleUser.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface DribbbleUser : MTLModel <MTLJSONSerializing>

NS_ASSUME_NONNULL_BEGIN

@property (nonatomic, assign, readonly) NSUInteger identifier;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSURL *avatarURL;

NS_ASSUME_NONNULL_END

@end
