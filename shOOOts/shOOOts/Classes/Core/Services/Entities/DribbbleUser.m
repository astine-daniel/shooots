//
//  DribbbleUser.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleUser.h"

#import "MTLJSONAdapter+Transformers.h"

static NSString * const kDribbbleUserIdentifierJSONKey = @"id";
static NSString * const kDribbbleUserNameJSONKey = @"name";
static NSString * const kDribbbleUserAvatarURLJSONKey = @"avatar_url";

@implementation DribbbleUser

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    DribbbleUser *user = nil;
    return @{@keypath(user.identifier): kDribbbleUserIdentifierJSONKey,
              @keypath(user.name): kDribbbleUserNameJSONKey,
              @keypath(user.avatarURL): kDribbbleUserAvatarURLJSONKey};
}

+ (NSValueTransformer *)identifierJSONTransformer {
    return [MTLJSONAdapter unsignedIntegerTransformer];
}

+ (NSValueTransformer *)avatarURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end
