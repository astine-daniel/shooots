//
//  DribbbleImage.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleImage.h"

static NSString * const kDribbbleImageURLJSONKey = @"normal";
static NSString * const kDribbbleImageHiDPIURLJSONKey = @"hidpi";
static NSString * const kDribbbleImageTeaserURLJSONKey = @"teaser";

@implementation DribbbleImage

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    DribbbleImage *image = nil;
    return @{@keypath(image.URL): kDribbbleImageURLJSONKey,
              @keypath(image.HiDPIURL): kDribbbleImageHiDPIURLJSONKey,
              @keypath(image.teaserURL): kDribbbleImageTeaserURLJSONKey};
}

+ (NSValueTransformer *)URLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)HiDPIURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)teaserURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end
