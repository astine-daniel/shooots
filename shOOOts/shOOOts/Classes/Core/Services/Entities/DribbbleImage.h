//
//  DribbbleImage.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface DribbbleImage : MTLModel <MTLJSONSerializing>

NS_ASSUME_NONNULL_BEGIN

@property (nonatomic, strong, readonly) NSURL *HiDPIURL;
@property (nonatomic, strong, readonly) NSURL *URL;
@property (nonatomic, strong, readonly) NSURL *teaserURL;

NS_ASSUME_NONNULL_END

@end
