//
//  DribbbleAPI+AccessToken.m
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleAPI+AccessToken.h"

static NSString * const kDribbbleAPIAccessTokenKey = @"DribbbleAPIAccessToken";

@implementation DribbbleAPI (AccessToken)

+ (NSString *)accessToken {
    NSString *accessToken = (NSString *)[[NSBundle mainBundle] objectForInfoDictionaryKey:kDribbbleAPIAccessTokenKey];
    if (accessToken == nil) {
        return @"";
    }
    
    return accessToken;
}

@end
