//
//  DribbbleAPI+ShotAPI.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import <DribbbleAPI/DribbbleAPI.h>
#import "ShotAPI.h"

@interface DribbbleAPI (ShotAPI) <ShotAPI>

@end
