//
//  DribbbleAPI+AccessToken.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import <DribbbleAPI/DribbbleAPI.h>

@interface DribbbleAPI (AccessToken)

NS_ASSUME_NONNULL_BEGIN

+ (NSString *)accessToken;

NS_ASSUME_NONNULL_END

@end
