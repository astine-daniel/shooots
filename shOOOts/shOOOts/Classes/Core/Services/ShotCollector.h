//
//  ShotCollector.h
//  shOOOts
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@class Shot;
@class DribbbleShot;

@interface ShotCollector : NSObject

NS_ASSUME_NONNULL_BEGIN

- (void)addDribbbleShots:(NSArray<DribbbleShot *> *)dribbbleShots;
- (NSArray<Shot *> *)collectedShots;

NS_ASSUME_NONNULL_END

@end
