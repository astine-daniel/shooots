//
//  NSString+Empty.h
//  DribbbleAPI
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@interface NSString (Empty)

- (BOOL)isEmpty;

@end
