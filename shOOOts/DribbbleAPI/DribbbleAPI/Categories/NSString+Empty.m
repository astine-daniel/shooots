//
//  NSString+Empty.m
//  DribbbleAPI
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "NSString+Empty.h"

@implementation NSString (Empty)

- (BOOL)isEmpty {
    NSCharacterSet *whitespaceAndNewlineCharacterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:whitespaceAndNewlineCharacterSet];
    return trimmedString.length == 0;
}

@end
