//
//  DribbbleShotMethod.h
//  DribbbleAPI
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;
#import <DribbbleAPI/DribbbleAPIMethod.h>

@interface DribbbleShotMethod : NSObject <DribbbleAPIMethod>

+ (DribbbleShotMethod *)shotMethod;

@property (nonatomic, assign) NSUInteger page;

@end
