//
//  DribbbleShotMethod.m
//  DribbbleAPI
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleShotMethod.h"

static NSString * const kDribbbleAPIPageParameterName = @"page";

@implementation DribbbleShotMethod

+ (DribbbleShotMethod *)shotMethod {
    return [[DribbbleShotMethod alloc] init];
}

- (NSString *)name {
    return @"shots";
}

- (NSDictionary *)parameters {
    return @{kDribbbleAPIPageParameterName: @(self.page)};
}

@end
