//
//  DribbbleAPIObject.h
//  DribbbleAPI
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@protocol DribbbleAPIMethod;

@interface DribbbleAPI : NSObject

NS_ASSUME_NONNULL_BEGIN

- (instancetype) __unavailable init;
- (instancetype)initWithAccessToken:(NSString *)accessToken;

- (void)shotsFromPage:(NSUInteger)page
              success:(void (^)(NSArray *data))success
              failure:(void (^)(NSError *error))failure;

- (void)callMethod:(id<DribbbleAPIMethod>)method
           success:(void (^)(NSArray *data))success
           failure:(void (^)(NSError *error))failure;

NS_ASSUME_NONNULL_END

@end
