//
//  DribbbleHTTPClient.h
//  DribbbleAPI
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import AFNetworking;

@interface DribbbleHTTPClient : AFHTTPSessionManager

NS_ASSUME_NONNULL_BEGIN

+ (DribbbleHTTPClient *)httpClient;
- (instancetype)init;

- (void)requestWithMethod:(NSString *)method
               parameters:(NSDictionary *)parameters
                  success:(void (^)(NSArray *response))success
                  failure:(void (^)(NSError *error))failure;

NS_ASSUME_NONNULL_END

@end
