//
//  DribbbleHTTPClient.m
//  DribbbleAPI
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleHTTPClient.h"

static NSString *const kDribbbleAPIBaseURLString = @"https://api.dribbble.com/v1/";

@implementation DribbbleHTTPClient

+ (DribbbleHTTPClient *)httpClient {
    return [[DribbbleHTTPClient alloc] init];
}

- (instancetype)init {
    self = [super initWithBaseURL:[NSURL URLWithString:kDribbbleAPIBaseURLString]];
    if (!self) {
        return nil;
    }
    
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    
    return self;
}

- (void)requestWithMethod:(NSString *)method
               parameters:(NSDictionary *)parameters
                  success:(void (^)(NSArray * _Nonnull))success
                  failure:(void (^)(NSError * _Nonnull))failure {
    
    [self GET:method
   parameters:parameters
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
          success(responseObject);
      }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          failure(error);
      }];
}

@end
