//
//  DribbbleAPIMethod.h
//  DribbbleAPI
//
//  Created by Daniel Astine on 13/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

@import Foundation;

@protocol DribbbleAPIMethod <NSObject>

@property (nonatomic, copy, readonly) NSDictionary *parameters;
@property (nonatomic, copy, readonly) NSString *name;

@end
