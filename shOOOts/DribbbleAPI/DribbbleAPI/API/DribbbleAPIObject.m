//
//  DribbbleAPIObject.m
//  DribbbleAPI
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright © 2015 Daniel Astine. All rights reserved.
//

#import "DribbbleAPIObject.h"

#import <DribbbleAPI/DribbbleAPIMethod.h>

#import "NSString+Empty.h"
#import "DribbbleHTTPClient.h"
#import "DribbbleShotMethod.h"

static NSString * const kDribbbleAPIAccessTokenParameterName = @"access_token";

@interface DribbbleAPI ()

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, strong) DribbbleHTTPClient *httpClient;

@end

@implementation DribbbleAPI

- (instancetype)initWithAccessToken:(NSString *)accessToken {
    self = [super init];
    if (!self || [accessToken isEmpty]) {
        return nil;
    }
    
    self.accessToken = accessToken;
    
    return self;
}

- (void)shotsFromPage:(NSUInteger)page
              success:(void (^)(NSArray * _Nonnull))success
              failure:(void (^)(NSError * _Nonnull))failure {
    DribbbleShotMethod *shotMethod = [DribbbleShotMethod shotMethod];
    shotMethod.page = page;
    
    [self callMethod:shotMethod success:success failure:failure];
}

- (void)callMethod:(id<DribbbleAPIMethod>)method
           success:(void (^)(NSArray * _Nonnull))success
           failure:(void (^)(NSError * _Nonnull))failure {
    
    NSDictionary *parameters = [self parametersWithAccessToken:method.parameters];
    [self.httpClient requestWithMethod:method.name
                            parameters:parameters
                               success:success
                               failure:failure];
}

- (NSDictionary *)parametersWithAccessToken:(NSDictionary *)parameters {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.accessToken forKey:kDribbbleAPIAccessTokenParameterName];
    [params addEntriesFromDictionary:parameters];
    
    return [params copy];
}

- (DribbbleHTTPClient *)httpClient {
    if (_httpClient == nil) {
        _httpClient = [DribbbleHTTPClient httpClient];
    }
    
    return _httpClient;
}

@end
