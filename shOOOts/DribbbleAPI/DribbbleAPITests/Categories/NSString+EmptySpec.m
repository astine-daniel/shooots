//
//  NSString+NilOrEmptySpec.m
//  DribbbleAPI
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright 2015 Daniel Astine. All rights reserved.
//

// Class under test
#import "NSString+Empty.h"

// Test support
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>


SpecBegin(NSString)

describe(@"NSString+Empty", ^{
    describe(@"isEmpty", ^{
        it(@"should return YES to an empty string", ^{
            NSString *string = @"";
            expect([string isEmpty]).to.beTruthy();
        });
        
        it(@"should return YES to a new line string", ^{
            NSString *string = @"\n";
            expect([string isEmpty]).to.beTruthy();
        });
        
        it(@"should return NO to string with content", ^{
           NSString *string = @"string";
            expect([string isEmpty]).to.beFalsy();
        });
    });
});

SpecEnd
