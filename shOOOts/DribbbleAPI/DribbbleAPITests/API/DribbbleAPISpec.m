//
//  DribbbleAPISpec.m
//  DribbbleAPI
//
//  Created by Daniel Astine on 12/11/15.
//  Copyright 2015 Daniel Astine. All rights reserved.
//

// Class under test
#import <DribbbleAPI/DribbbleAPI.h>

// Collaborators
#import "DribbbleHTTPClient.h"

// Test support
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>

#pragma mark - Test Utils

@interface DribbbleAPI (Test)

@property (nonatomic, strong) DribbbleHTTPClient *httpClient;

@end

@interface TestMethod : NSObject <DribbbleAPIMethod>

@property (nonatomic, copy) NSDictionary *parameters;
@property (nonatomic, copy) NSString *name;

+ (TestMethod *)methodWithName:(NSString *)name parameters:(NSDictionary *)parameters;

@end

@implementation TestMethod

+ (TestMethod *)methodWithName:(NSString *)name parameters:(NSDictionary *)parameters {
    TestMethod *method = [[TestMethod alloc] init];
    method.name = name;
    method.parameters = parameters;
    
    return method;
}

@end

#pragma mark

SpecBegin(DribbbleAPI)

describe(@"DribbbleAPI", ^{
    
    __block DribbbleAPI *api;
    __block DribbbleHTTPClient *httpClientMock;
    __block TestMethod *method;
    
    beforeEach(^{
        httpClientMock = [DribbbleHTTPClient httpClient];
        httpClientMock = OCMPartialMock(httpClientMock);
        
        api = [[DribbbleAPI alloc] initWithAccessToken:@"token"];
        api.httpClient = httpClientMock;
        
        method = [TestMethod methodWithName:@"test" parameters:@{}];
    });
    
    context(@"when created", ^{
        beforeEach(^{
            api = [[DribbbleAPI alloc] initWithAccessToken:@""];
        });
        
        it(@"return nil for an empty access token", ^{
            expect(api).to.beNil();
        });
    });
    
    context(@"when request for shots", ^{
        it(@"should return any data", ^{
           OCMExpect([httpClientMock requestWithMethod:[OCMArg any]
                                            parameters:[OCMArg any]
                                               success:[OCMArg any]
                                               failure:[OCMArg any]]).andDo(^ (NSInvocation *invocation) {
               void (^success)(NSArray *response) = nil;
               [invocation getArgument:&success atIndex:4];
               success(@[]);
           });
           
           waitUntil(^(DoneCallback done) {
               void(^success)(NSArray *) = ^(NSArray *response) {
                   expect(response).toNot.beNil();
                   done();
               };
               
               [api shotsFromPage:1 success:success failure:[OCMArg any]];
           });
       });
        
       it(@"should return an error in case of failed", ^{
           OCMExpect([httpClientMock requestWithMethod:[OCMArg any]
                                            parameters:[OCMArg any]
                                               success:[OCMArg any]
                                               failure:[OCMArg any]]).andDo(^ (NSInvocation *invocation) {
               void (^failureCallback)(NSError *error) = nil;
               [invocation getArgument:&failureCallback atIndex:5];
               failureCallback([NSError errorWithDomain:@"shots" code:0 userInfo:nil]);
           });
           
           waitUntil(^(DoneCallback done) {
               void(^failureCallback)(NSError *) = ^(NSError *error) {
                   expect(error).toNot.beNil();
                   expect(error.domain).to.equal(@"shots");
                   
                   done();
               };
               
               [api shotsFromPage:1 success:[OCMArg any] failure:failureCallback];
           });
       });
    });
    
    context(@"when call method", ^{
        it(@"should return response", ^{
            OCMExpect([httpClientMock requestWithMethod:method.name
                                             parameters:[OCMArg any]
                                                success:[OCMArg any]
                                                failure:[OCMArg any]]).andDo(^ (NSInvocation *invocation) {
                void (^success)(NSArray *response) = nil;
                [invocation getArgument:&success atIndex:4];
                success(@[]);
            });
            
            waitUntil(^(DoneCallback done) {
                void(^success)(NSArray *) = ^(NSArray *response) {
                    expect(response).toNot.beNil();
                    done();
                };
                
                [api callMethod:method success:success failure:[OCMArg any]];
            });
        });
        
        it(@"should return an error in case of failed", ^{
            OCMExpect([httpClientMock requestWithMethod:method.name
                                             parameters:[OCMArg any]
                                                success:[OCMArg any]
                                                failure:[OCMArg any]]).andDo(^ (NSInvocation *invocation) {
                void (^failureCallback)(NSError *error) = nil;
                [invocation getArgument:&failureCallback atIndex:5];
                failureCallback([NSError errorWithDomain:@"shots" code:0 userInfo:nil]);
            });
            
            waitUntil(^(DoneCallback done) {
                void(^failureCallback)(NSError *) = ^(NSError *error) {
                    expect(error).toNot.beNil();
                    expect(error.domain).to.equal(@"shots");
                    
                    done();
                };
                
                [api callMethod:method success:[OCMArg any] failure:failureCallback];
            });
        });
    });
});

SpecEnd
